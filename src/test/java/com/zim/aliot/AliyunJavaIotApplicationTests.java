package com.zim.aliot;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestClientException;

import com.zim.aliot.service.CurlService;
import com.zim.aliot.vo.Book;
import com.zim.aliot.vo.ResultVO;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AliyunJavaIotApplicationTests {
    @Autowired
    private CurlService curlService;
    
	@Test
	public void contextLoads() {
	    System.out.println("嘤嘤嘤");
	}
	
	@Test
	public void curlXml() {
	    String res = curlService.someRestCall("ddd=222");
	    System.out.println(res);
	}
	
	@Test
	public void curlPost() {
	    Book request = new Book();
	    request.setName("三国演义");
	    request.setAuthor("罗贯中");
	    request.setPrice(90);
	    LinkedMultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
	    headers.add("sign", "ddddd");
	    
	   HttpHeaders httpHeaders = curlService.jsonHeaders(headers);
	   HttpEntity<Book> req = new HttpEntity<>(request,httpHeaders);
	   ParameterizedTypeReference<ResultVO<Book>> myBean = new ParameterizedTypeReference<ResultVO<Book>>() {};
        
        try {
            ResponseEntity<ResultVO<Book>> res = curlService.getRestTemplate().exchange("http://centos7.com", HttpMethod.POST, req, myBean);
            System.out.println(res.getBody());
        } catch (RestClientException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
	}

}
