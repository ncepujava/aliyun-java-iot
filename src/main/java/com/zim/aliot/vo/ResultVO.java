package com.zim.aliot.vo;

import lombok.Data;

/**
 * 全局api返回数据
 * @author zi-m.cn
 *
 * @param <T>
 */
@Data
public class ResultVO<T> { 
    /** 错误码. */
    private Integer code;

    /** 提示信息. */
    private String msg;

    /** 具体内容. */
    private T data;
    

}
