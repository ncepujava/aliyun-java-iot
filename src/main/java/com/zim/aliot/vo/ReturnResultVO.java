package com.zim.aliot.vo;

import lombok.Data;

@Data
public class ReturnResultVO<T> {
	
	/** 错误码. */
    private Integer result;

    /** 提示信息. */
    private String msg;

    /** 具体内容. */
    private T data;
}
