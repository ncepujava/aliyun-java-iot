package com.zim.aliot.dao;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

import com.zim.aliot.vo.VehicleVO;

@Repository
public interface ReactiveVehicleRepository extends ReactiveMongoRepository<VehicleVO, String> {

}
