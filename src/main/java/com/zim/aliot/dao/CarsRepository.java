package com.zim.aliot.dao;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.zim.aliot.dataobject.CarsDo;

@Repository
public interface CarsRepository extends MongoRepository<CarsDo, String> {

    CarsDo findByDeviceName(String deviceName);
    
    CarsDo findByVin(String vin);
    

    
    
}
