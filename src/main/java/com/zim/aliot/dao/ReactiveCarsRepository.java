package com.zim.aliot.dao;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

import com.zim.aliot.dataobject.CarsDo;


@Repository
public interface ReactiveCarsRepository extends ReactiveMongoRepository<CarsDo, String> {

}
