package com.zim.aliot.dao;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.zim.aliot.vo.VehicleVO;

@Repository
public interface VehicleRepository extends MongoRepository<VehicleVO, String> {

}
