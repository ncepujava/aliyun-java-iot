package com.zim.aliot.async;

import java.util.Base64;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.zim.aliot.client.LcxClient;
import com.zim.aliot.dataobject.CarsDo;
import com.zim.aliot.dataobject.CommandDo;
import com.zim.aliot.lcx.LcxResult;
import com.zim.aliot.service.CarsService;
import com.zim.aliot.service.CommandService;
import com.zim.analysis.po.GpsPO;
import com.zim.analysis.po.PhotoPO;
import com.zim.analysis.util.FormatUtils;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class NotifyCommand {
	private final Base64.Encoder encode = Base64.getEncoder();
	
	@Autowired
    private CarsService carsService;
	
	@Autowired
	private CommandService commandService;
	
	@Autowired
	private LcxClient lcxClient;
	
    @Autowired
    private MongoTemplate mongoTemplate;
	
	@Async
	public void ackNotify(String vin,String command,int serialNumber,boolean commandResult,String reason) {
		CommandDo commandDo = commandService.find(vin, command, serialNumber);
		
		if(null == commandDo) {
			log.error("ack seqid null:{}",command);
			return;
		}else {
			//推送客户端
			LcxResult result = lcxClient.acceptInform(commandDo.getSeqId(), commandResult, commandDo.getTime(), reason);
			
			if(null != result && 0 == result.getResult()) {
				commandService.updateStatus(vin, command, serialNumber, 1);
			}else {
				log.error("[ ackNotify ]:{}",result);
				commandService.updateStatus(vin, command, serialNumber, 0);
			}
			
		}

	}
	
	@Async
	public void bluePassNotify(String vin,String command,int serialNumber,boolean commandResult) {
		CommandDo commandDo = commandService.find(vin, command, serialNumber);
		
		if(null == commandDo) {
			log.error("ack seqid null:{}",command);
			return;
		}else {
			//推送客户端
			LcxResult result = lcxClient.acceptBlueTooth(commandResult, vin, commandDo.getPassword(), commandDo.getTime());
			
			if(null != result && 0 == result.getResult()) {
				commandService.updateStatus(vin, command, serialNumber, 1);
			}else {
				log.error("[ 蓝牙密码通知 ]:{}",result);
				commandService.updateStatus(vin, command, serialNumber, 0);
			}
			
		}
	}
	
	@Async
	public void eventSend(GpsPO gpsPO,String event,String action) {
		CarsDo carsInfo = null;
		String vin = "";
		try {
			
			carsInfo = carsService.find(gpsPO.getDeviceName());
		} catch (Exception e) {
			log.error("[ 设备号不存在 ]:{}",e.getMessage());
		}
		if(null != carsInfo) {
			vin = carsInfo.getVin();
		}
		LcxResult result = lcxClient.sendEvent(vin, event, action, gpsPO.getTime(), System.currentTimeMillis());
		log.info("事件推送数据:{}",result);
	}
	
	@Async
	public void waringEvent(GpsPO gpsPO) {
		CarsDo carsInfo = null;
		String vin = "";
		try {
			
			carsInfo = carsService.find(gpsPO.getDeviceName());
		} catch (Exception e) {
			log.error("[ 设备号不存在 ]:{}",e.getMessage());
		}
		if(null != carsInfo) {
			vin = carsInfo.getVin();
		}
		LcxResult lcxResult = lcxClient.sendWarning(vin, "OBD", gpsPO.getObdStatus()==0?"pull_out":"insert", gpsPO.getTime(), System.currentTimeMillis());
		log.info("OBD报警:{}",lcxResult);
	}
	
	@Async
	public void saveImage(String vin,String image,PhotoPO photo) {
		
		byte[] imageByte = FormatUtils.strToByte(image);
		
		String base64Str = encode.encodeToString(imageByte);
		
		CommandDo commandDo = null;
		
		Criteria criteria = null;
		try {
			criteria  = Criteria.where("vin").is(vin).
					and("command").is("PHOTO").
					and("status").is(0);
			commandDo = mongoTemplate.findOne(new Query(criteria), CommandDo.class);
		} catch (Exception e) {
			log.error("[查找数据失败]:{}",e.getMessage());
		}
		String seqId;
		if(null == commandDo) {
			seqId = String.valueOf(System.currentTimeMillis());
			
		}else {
			seqId = commandDo.getSeqId();
		}
												
		LcxResult result = lcxClient.sendPhotoEvent(seqId, base64Str, String.valueOf(photo.getLongitude()), String.valueOf(photo.getLatitude()), System.currentTimeMillis());
//		log.info("[ 图片保存 ]:{}",result);
		if(null != result && 0 == result.getResult()) {
			mongoTemplate.upsert(new Query(criteria), new Update().set("status", 1), CommandDo.class);
		}else {
			mongoTemplate.upsert(new Query(criteria), new Update().set("status", 2), CommandDo.class);
		}
		 
	}
	
}
