package com.zim.aliot.constant;

/**
 * redis常量
 * @author zi-m.cn
 */
public class RedisConstant {
    
    /**
     * EXPIRE key seconds
     */
    public static int EXPIRE = 7200;
    
}
