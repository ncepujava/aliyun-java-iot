package com.zim.aliot.listener;

import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class StopListener  {
    @Autowired
    private StartCommandLineRunner startCommandLineRunner; 
    
    @PreDestroy
    public void destory() {

        startCommandLineRunner.stop();
        System.out.println("停止");
    }

}
