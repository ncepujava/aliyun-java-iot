package com.zim.aliot.exception;

public class ValueAbsentException extends Throwable {

    /**
     * 
     */
    private static final long serialVersionUID = -8183312297094760322L;

    public ValueAbsentException() {
      super();
    }

    public ValueAbsentException(String msg) {
      super(msg);
    }

    @Override
    public String getMessage() {
      return "No value present in the Optional instance";
    }
  }