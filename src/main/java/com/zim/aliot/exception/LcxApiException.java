package com.zim.aliot.exception;

import com.zim.aliot.enums.ResultEnum;

public class LcxApiException extends RuntimeException {

	/**
     * 自定义异常类
     */
	private static final long serialVersionUID = 7906322736689471643L;
   
    private int code;
    
    public LcxApiException(ResultEnum resultEnum) {
        super(resultEnum.getMessage());

        this.setCode(resultEnum.getCode());
    }
    
    public LcxApiException(int code, String message) {
        super(message);
        this.setCode(code);
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
