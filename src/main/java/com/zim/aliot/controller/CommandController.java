package com.zim.aliot.controller;

import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.aliyuncs.utils.StringUtils;
import com.zim.aliot.form.JoyCommandForm;
import com.zim.aliot.form.RentTypeForm;
import com.zim.aliot.form.TakePhotoForm;
import com.zim.aliot.service.CommandService;
import com.zim.aliot.util.ResultVOUtils;
import com.zim.aliot.vo.ResultVO;
import com.zim.aliot.vo.ReturnResultVO;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/command")
@Slf4j
public class CommandController {
	
	@Autowired
	private CommandService commandService;
	
	@RequestMapping(value = "/doCommandAction", method = { RequestMethod.POST,
            RequestMethod.GET }, produces = "application/json;charset=UTF-8")
    public ReturnResultVO<String> doCommandAction(@Valid JoyCommandForm joyCommandForm,BindingResult bindingResult){
		ReturnResultVO<String> res = new ReturnResultVO<String>();
		if(bindingResult.hasErrors()) {
			log.error("【车辆控制】:参数不正确{}",joyCommandForm);
			res.setResult(1);
			res.setMsg("参数错误");
			return res;
		}
		
		boolean resCommandAction = commandService.commandAction(joyCommandForm.getCar_unique_id(), joyCommandForm.getSeq_id(), joyCommandForm.getCommand(), joyCommandForm.getAction(), joyCommandForm.getTime(),0,"","");
		
		if(resCommandAction) {
			res.setResult(0);
			res.setMsg("指令下发成功");
		}else {
			res.setResult(-11);
			res.setMsg("指令下发失败");
		}

    	return res;
    	
    }
	
	@RequestMapping(value = "/doNotifyRentType", method = { RequestMethod.POST,
            RequestMethod.GET }, produces = "application/json;charset=UTF-8")
    public ReturnResultVO<String> doNotifyRentType(@Valid RentTypeForm rentTypeForm,BindingResult bindingResult){
		ReturnResultVO<String> res = new ReturnResultVO<String>();
		if(bindingResult.hasErrors()) {
			log.error("【 车辆租赁状态 】:参数不正确{}",rentTypeForm);
			res.setResult(1);
			res.setMsg("参数错误");
			return res;
		}
		long time = new Date().getTime();
		boolean resRentType = commandService.commandAction(rentTypeForm.getCar_unique_id(), "", "RENT", 0, time, rentTypeForm.getType(),"","");
		
		if(resRentType) {
			res.setResult(0);
			res.setMsg("成功");
		}else {
			res.setResult(-11);
			res.setMsg("指令下发失败");
		}

    	return res;	
    	
    }

	@RequestMapping(value = "/doTakePhoto", method = { RequestMethod.POST,
            RequestMethod.GET }, produces = "application/json;charset=UTF-8")
    public ReturnResultVO<String> doTakePhoto(@Valid TakePhotoForm takePhotoForm,BindingResult bindingResult){
		ReturnResultVO<String> res = new ReturnResultVO<String>();
		if(bindingResult.hasErrors()) {
			log.error("【 拍照 】:参数不正确{}",takePhotoForm);
			res.setResult(1);
			res.setMsg("参数错误");
			return res;
		}
		boolean resTakePhoto = commandService.commandAction(takePhotoForm.getCar_unique_id(), takePhotoForm.getSeq_id(), "PHOTO", 0, takePhotoForm.getTime(), 0,"","");
		if(resTakePhoto) {
			res.setResult(0);
			res.setMsg("成功");
		}else {
			res.setResult(-11);
			res.setMsg("指令下发失败");
		}

    	return res;
    	
    }
	
	@RequestMapping(value = "/doSetCarEnv", method = { RequestMethod.POST,
            RequestMethod.GET }, produces = "application/json;charset=UTF-8")
    public ResultVO<Object> doSetCarEnv(){
    	return ResultVOUtils.success("切换环境");
    	
    }
	
	
	@RequestMapping(value = "/doModifyBlueToothPwd", method = { RequestMethod.POST,
            RequestMethod.GET }, produces = "application/json;charset=UTF-8")
    public ReturnResultVO<String> doModifyBlueToothPwd(@RequestParam("car_unique_id") String car_unique_id,@RequestParam("pwd") String pwd){
		ReturnResultVO<String> res = new ReturnResultVO<String>();
		if(StringUtils.isEmpty(car_unique_id) || StringUtils.isEmpty(pwd)) {
			log.error("【 更改蓝牙密码 】:参数不能为空car_unique_id:{},pwd:{}",car_unique_id,pwd);
			res.setResult(1);
			res.setMsg("参数错误");
			return res;
		}
		long time = new Date().getTime();
		boolean resBlueTooth = commandService.commandAction(car_unique_id, "", "BLUETOOTH", 0, time, 0,"",pwd);
		if(resBlueTooth) {
			res.setResult(0);
			res.setMsg("成功");
		}else {
			res.setResult(-11);
			res.setMsg("指令下发失败");
		}

    	return res;
    	
    }
	
	@RequestMapping(value = "/doCallCar", method = { RequestMethod.POST,
            RequestMethod.GET }, produces = "application/json;charset=UTF-8")
    public ReturnResultVO<String> doCallCar(@RequestParam("car_unique_id") String car_unique_id){
		ReturnResultVO<String> res = new ReturnResultVO<String>();
		if(StringUtils.isEmpty(car_unique_id)) {
			log.error("【 车况点名 】:参数不能为空{}",car_unique_id);
			res.setResult(1);
			res.setMsg("参数错误");
			return res;
		}
		long time = new Date().getTime();
		boolean resCall = commandService.commandAction(car_unique_id, "", "CALL", 0, time, 0,"","");
		if(resCall) {
			res.setResult(0);
			res.setMsg("成功");
		}else {
			res.setResult(-11);
			res.setMsg("指令下发失败");
		}

    	return res;
    	
    	
    }
	
	
}
