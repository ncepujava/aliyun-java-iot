package com.zim.aliot.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import com.aliyun.mns.client.CloudAccount;
import com.aliyun.mns.client.CloudQueue;
import com.aliyun.mns.client.MNSClient;
import com.zim.aliot.config.AliMnsConfig;

@Component
public class AliMnsClient {
    @Autowired
    private AliMnsConfig aliMnsConfig;
    
    @Bean
    public MNSClient sMNSClient() {
        CloudAccount account = new CloudAccount(aliMnsConfig.getAccessKey(),aliMnsConfig.getAccessSecret(),aliMnsConfig.getEndpoint());
        return account.getMNSClient();
    }
    
    @Bean
    private CloudQueue cloudQueue() {
        return sMNSClient().getQueueRef(aliMnsConfig.getQueueName());
    }
}
