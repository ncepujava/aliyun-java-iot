package com.zim.aliot.client;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;
import com.zim.aliot.config.AliIotConfig;

@Component
public class AliIotClient {
    @Autowired AliIotConfig aliIotConfig;
    
    @Bean
    public DefaultAcsClient defaultAcsClient() {
            DefaultProfile.addEndpoint("cn-shanghai", "Iot", "iot.cn-shanghai.aliyuncs.com");
            IClientProfile profile = DefaultProfile.getProfile("cn-shanghai", aliIotConfig.getAccessKey(),aliIotConfig.getAccessSecret());
            return new DefaultAcsClient(profile);
    }
    
}
