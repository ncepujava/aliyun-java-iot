package com.zim.aliot.schedules;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.mongodb.client.result.DeleteResult;
import com.zim.aliot.dataobject.CommandDo;
import com.zim.aliot.util.RedisLockUtils;
import com.zim.aliot.vo.VehicleVO;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class HisGpsInfo {
	
	@Autowired
	private RedisLockUtils redisLockUtils;
	
    @Autowired
    private MongoTemplate mongoTemplate;
	
	@Scheduled(cron = "0 0 3 * * *") 
    public void transferMg() {
		long time = System.currentTimeMillis()+300000;
		if(!redisLockUtils.lock("remove", String.valueOf(time))) {
			log.info("已经被锁");
			return;
		}
		Criteria criteria  = Criteria.where("time").lt(System.currentTimeMillis()-259200000);
		DeleteResult res = mongoTemplate.remove(new Query(criteria), VehicleVO.class);
		Criteria commandDoCriteria  = Criteria.where("time").lt(System.currentTimeMillis()-10800000);
		DeleteResult comDeleteResult = mongoTemplate.remove(new Query(commandDoCriteria), CommandDo.class);
		log.info("VehicleVO删除了{}条,CommandDo删除了{}条",res.getDeletedCount(),comDeleteResult.getDeletedCount());
		
		redisLockUtils.unlock("remove", String.valueOf(time));
	}
}
