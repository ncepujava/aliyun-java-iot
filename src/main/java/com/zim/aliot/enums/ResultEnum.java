package com.zim.aliot.enums;

import lombok.Getter;

@Getter
public enum ResultEnum {
    
    /**
     * 成功
     */
    SUCCESS(0, "成功"),
    
    /**
     * 请求错误
     */
    ERROR(1,"请求错误"),

    /**
     * 参数错误
     */
    PARAM_ERROR(2, "参数错误"),
    
    /**
     * 对方服务器错误
     */
    CURL_ERROR(3,"对方服务错误"),
    
    /**
     * 登录失败
     */
    LOGIN_FAIL(10, "身份验证失败"),

    /**
     * 退出成功
     */
    LOGOUT_SUCCESS(11, "登出成功"),
    
    /**
     * 添加失败
     */
    WRITE_ERROR(20,"添加失败"), 
	
	NOTFIND_VIN(4,"未找到该车辆信息，请绑定"),
	
	OFF_LINE(12,"车机离线");
    
    private Integer code;

    private String message;

    ResultEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
