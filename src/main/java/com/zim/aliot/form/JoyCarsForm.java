package com.zim.aliot.form;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class JoyCarsForm {
    
    /**
     * 设备号
     */
    @NotEmpty(message = "设备号必填")
    private String deviceName;
    
    /**
     * 车架
     */
    @NotEmpty(message = "车架必填")
    private String vin;
    
    /**
     * 车机供应商
     */
    @Min(value=1,message="供应商参数太小")
    @NotNull(message="供应商必填")
    private int supplierNo;
    
    private String bluetoothId;
    
    @NotEmpty(message = "车牌号必填")
    private String carSn;
}
