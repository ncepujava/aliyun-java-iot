package com.zim.aliot.form;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class JoyCommandForm {
	
	/**
	 * 车架号
	 */
	@NotEmpty(message = "车架号不能为空")
	private String car_unique_id;
	
	/**
	 * 请求唯一标识（32位字符串）
	 */
	@NotEmpty(message = "请求唯一标识不能为空")
	private String seq_id;
	
	/**
	 * 指令
	 */
	@NotEmpty(message = "指令不能为空")
	private String command;
	
	/**
	 * 动作
	 */
	@NotNull(message = "动作不能为空")
	private int action;
	
	/**
	 * 下发命令时的时间戳
	 */
	@NotNull(message = "下发命令时的时间戳不能为空")
	private long time;

}
