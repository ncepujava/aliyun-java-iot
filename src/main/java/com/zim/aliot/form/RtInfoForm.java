package com.zim.aliot.form;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class RtInfoForm {
	
	@NotEmpty(message = "车架号不能为空")
	private String car_unique_id;
	
	@NotNull(message = "开始时间不能为空")
	private long timeStart;
	
	@NotNull(message = "结束时间不能为空")
	private long timeEnd;
}
