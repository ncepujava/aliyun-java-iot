package com.zim.aliot.form;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class RentTypeForm {
	
	@NotEmpty(message = "车架号不能为空")
	private String car_unique_id;
	
	@NotNull(message = "类型不能为空")
	private int type;
	
}
