package com.zim.aliot.handler;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zim.aliot.exception.IotApiException;
import com.zim.aliot.exception.LcxApiException;
import com.zim.aliot.util.ResultVOUtils;
import com.zim.aliot.vo.ResultVO;
import com.zim.aliot.vo.ReturnResultVO;

@ControllerAdvice
public class IotApiExceptionHandler {
    
    @ExceptionHandler(value = IotApiException.class)
    @ResponseBody
    public ResultVO<Object> handlerAuthorizeException(IotApiException e){
        return ResultVOUtils.error(e.getCode(), e.getMessage());
        
    }
    
    @ExceptionHandler(value= LcxApiException.class)
    @ResponseBody
    public ReturnResultVO<String> handlerLcxException(LcxApiException e) {
    	
    	  ReturnResultVO<String> returnResultVo = new ReturnResultVO<>();
    	  returnResultVo.setResult(e.getCode());
    	  returnResultVo.setMsg(e.getMessage());
    	  return returnResultVo;
		
	}
    
}
