package com.zim.aliot.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

/**
 * 赖床讯客户端配置
 * @author zi-m.cn
 */
@Data
@Component
@ConfigurationProperties(prefix = "lcx")
public class LcxConfig {
    /**
     * 请求l c x主域名
     */
    private String clientUrl;
    
    /**
     * 本机IP
     */
    private String localIp;
    
    /**
     * 本机用户名
     */
    private String localUsername;
    
    /**
     * 本机秘钥
     */
    private String localKey;
}
