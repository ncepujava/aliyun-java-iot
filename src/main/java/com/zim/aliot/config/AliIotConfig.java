package com.zim.aliot.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
@ConfigurationProperties(prefix = "iot")
public class AliIotConfig {
    
    /**
     * id
     */
    private String accessKey;

    /**
     * 密钥
     */
    private String accessSecret;

    /**
     * 区域地址
     */
    private String productKey;
}
