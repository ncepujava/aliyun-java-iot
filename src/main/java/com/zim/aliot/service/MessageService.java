package com.zim.aliot.service;

public interface MessageService {
    
    /**
     * 解析车机数据
     * @param bytes
     * @throws Exception 
     */
    void messageHandle(byte[] bytes) throws Exception;
}
