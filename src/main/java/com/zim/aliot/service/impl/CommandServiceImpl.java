package com.zim.aliot.service.impl;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import com.zim.aliot.config.SaiKaConfig;
import com.zim.aliot.dao.CommandRepository;
import com.zim.aliot.dataobject.CarsDo;
import com.zim.aliot.dataobject.CommandDo;
import com.zim.aliot.service.AliClientService;
import com.zim.aliot.service.CarsService;
import com.zim.aliot.service.CommandService;
import com.zim.analysis.SaiKa;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CommandServiceImpl implements CommandService{
	
	@Autowired
    private AliClientService aliClientService;
	
	@Autowired
	private CommandRepository commandRepository;
	
    
    @Resource
    private RedisTemplate<String, CarsDo> redisTemplate;
	
    @Autowired
    private CarsService carsService;
    
    @Autowired
    private SaiKaConfig saiKaConfig;
    
    @Autowired
    private MongoTemplate mongoTemplate;


	@Override
	public boolean commandAction(String vin, String seqId, String command, int action, long time,int type,String version,String pwd) {
		SaiKa terminal = new SaiKa();
		CarsDo carsDo = null;
		try {
			carsDo = carsService.findByVin(vin);
		} catch (Exception e) {
			log.error("【车架号查找数据失败】:{}",e.getMessage());
			return false;
		}
		
		String deviceName = null;
		
		if(carsDo != null) {
			deviceName = carsDo.getDeviceName();
		}else {
			log.info("【deviceName值为】:{}",deviceName);
			return false;
		}
		
		byte[] bt = null;
		try {
			switch (command) {
			case "LOCK":
				if(action == 1) {
					bt = terminal.lock(deviceName, true);
				}else if (action == 0) {
					bt = terminal.lock(deviceName, false);
				}
				break;
			case "IGNITE":
				
				bt = terminal.ignite(deviceName, action == 0?false:true);
					
				break;
			case "WHISTLE":
				bt = terminal.whistle(deviceName, true);
				break;
			case "RENT":
				bt = terminal.rent(deviceName, type);
				break;
			case "PHOTO":
				bt = terminal.photo(deviceName);
				break;
			case "BLUETOOTH":
				bt = terminal.password(deviceName, pwd);
				break;
			case "CALL":
				bt = terminal.heartBeat(deviceName);
				break;
			case "UPGRADE":
				bt = terminal.update(deviceName, version, saiKaConfig.getFtp());
				break;
			default:
				break;
			}
		} catch (Exception e) {
			log.error("[ 获取byte失败 ]:{}",e.getMessage());
		}
		
		
		if(bt != null) {
			boolean res = aliClientService.pubMessage(bt, deviceName);
			if(res) {
				CommandDo commandDo = new CommandDo();
				commandDo.setVin(vin);
				commandDo.setAction(action);
				commandDo.setCommand(command);
				commandDo.setSeqId(seqId);
				commandDo.setStatus(0);
				commandDo.setType(type);
				commandDo.setPassword(pwd);
				commandDo.setTime(time);
				commandDo.setVersion(version);
				commandDo.setSerialNumber(bt[11]);
				
				CommandDo resCommandDo = this.save(commandDo);
				if(resCommandDo != null) {
					return true;
				}else {
					return false;
				}
			}else {
				return false;
			}
		}else {
			log.info("【byte值为】:{}", bt);
			return false;
		}
		
	}



	@Override
	public CommandDo save(CommandDo commandDo) {
		try {
			return commandRepository.save(commandDo);
		} catch (Exception e) {
			log.error("[ 写入数据失败 ]:{}",e.getMessage());
			return null;
		}
		
	}



	@Override
	public CommandDo find(String vin, String command, int serialNumber) {
		
		try {
			Criteria criteria  = Criteria.where("vin").is(vin).
					and("command").is(command).
					and("serialNumber").is(serialNumber).
					and("status").is(0);
			CommandDo commandDo = mongoTemplate.findOne(new Query(criteria), CommandDo.class);
			if(commandDo != null) {
				return commandDo;
			}else {
				log.info("[下发指令数据查找]:{}",commandDo);
				return null;
			}
		} catch (Exception e) {
			log.error("[查找数据失败]:{}",e.getMessage());
			return null;
		}
				
	}


	@Override
	public void updateStatus(String vin,String command,int serialNumber,int status) {
		try {
			Criteria criteria  = Criteria.where("vin").is(vin).
					and("command").is(command).
					and("serialNumber").is(serialNumber).
					and("status").is(0);
			mongoTemplate.upsert(new Query(criteria), new Update().set("status", status), CommandDo.class); 
		} catch (Exception e) {
			log.error("[更新status失败]:{}",e.getMessage());
		
		}
		
	}


	
	
	
}
