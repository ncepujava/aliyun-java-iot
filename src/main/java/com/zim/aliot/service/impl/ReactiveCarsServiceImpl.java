package com.zim.aliot.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zim.aliot.dao.ReactiveCarsRepository;
import com.zim.aliot.dataobject.CarsDo;
import com.zim.aliot.service.ReactiveCarsService;

import reactor.core.publisher.Mono;

@Service
public class ReactiveCarsServiceImpl implements ReactiveCarsService {
    
    @Autowired
    private ReactiveCarsRepository reactiveCarsRepository;
    
    @Override
    public Mono<CarsDo> save(CarsDo joyCarsPO) {
        return reactiveCarsRepository.save(joyCarsPO);
    }


}
