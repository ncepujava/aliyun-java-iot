package com.zim.aliot.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zim.aliot.dao.VehicleRepository;
import com.zim.aliot.service.VehicleService;
import com.zim.aliot.vo.VehicleVO;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class VehicleServiceImpl implements VehicleService{

    @Autowired
    private VehicleRepository vehicleRepository;
    
    @Override
    public void save(VehicleVO vehicleVO) {
     
        try {
            vehicleRepository.save(vehicleVO);
        } catch (Exception e) {
           log.error("[mongo写入失败]:{}",e.getMessage());
      
        }
    }

}
