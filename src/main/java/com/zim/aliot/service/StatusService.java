package com.zim.aliot.service;

import com.zim.aliot.entity.IOTStatus;

public interface StatusService {
    void deviceInfo(IOTStatus iotStatus);
}
