package com.zim.aliot.service;

import com.zim.aliot.vo.VehicleVO;
import com.zim.analysis.po.GpsPO;

public interface DataFilterService {
    
    /**
     * 整车数据过滤与矫正
     * @return 
     */
    VehicleVO filterHandle(GpsPO gpsPO, VehicleVO oldVehicleVO);
}
