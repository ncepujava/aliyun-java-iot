package com.zim.aliot.dataobject;

import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;

@Document(collection = "RltCarDeviceMachine")
public class RltCarDeviceMachine implements Serializable {

	private static final long serialVersionUID = -3726960299590441460L;

	// 还可以生成索引
    @Indexed(name = "carUniqueId", direction = IndexDirection.DESCENDING)
    @Field
    private String carUniqueId;

    @Indexed(unique = true,name = "unique_deviceName", direction = IndexDirection.DESCENDING)
    @Field
    private String deviceName;

    @Field
    private String deviceSecret;

    @Field
    private String deviceId;

    @Field
    private String machineNum;

    @Field
    private String supplierNo;

    public String getSupplierNo() {
        return supplierNo;
    }

    public void setSupplierNo(String supplierNo) {
        this.supplierNo = supplierNo;
    }

    public String getCarUniqueId() {
        return carUniqueId;
    }

    public void setCarUniqueId(String carUniqueId) {
        this.carUniqueId = carUniqueId;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getDeviceSecret() {
        return deviceSecret;
    }

    public void setDeviceSecret(String deviceSecret) {
        this.deviceSecret = deviceSecret;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getMachineNum() {
        return machineNum;
    }

    public void setMachineNum(String machineNum) {
        this.machineNum = machineNum;
    }
}
