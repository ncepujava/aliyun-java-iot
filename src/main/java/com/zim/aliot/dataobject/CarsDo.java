package com.zim.aliot.dataobject;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;


import lombok.Data;

@Data
@Document(collection = "joy_cars")
public class CarsDo implements Serializable{

	/**
	 * 车机和车辆数据对应实体类
	 */
	private static final long serialVersionUID = 1L;
	
	/**
     * 设备号
     */
    @Id
    private String deviceName;
    
    /**
     * 车架
     */
    @Indexed(name = "vin", direction = IndexDirection.DESCENDING)
    private String vin;
    
    /**
     * 车机供应商
     */
    private int supplierNo;

}
